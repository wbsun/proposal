\section{Background and Related Work}
\label{sec:background}

In this section, we will examine the work related to GPU computing in
    system level software.
We start with the many-core operating system design, which shares the
    same goal with us: improving the system level performance with the
    modern many-core processors, such as multicore CPUs, GPUs and other
    heterogeneous processors.
Then we will take a look at the general purpose GPU computing realm,
    which is closely related to our research.
We will examine three areas of the general purpose GPU computing: the
    computing frameworks for GPUs, the low level operating system
    support for GPUs, and recent applications of GPUs to system level
    tasks.
The last area also serves as a draft of the survey of system level GPU
    applications that will appear in the final dissertation.
Our KGPU whitepaper~\cite{KGPU} provides more details of existing and
    potential system level GPU applications, which are tailored and
    simplified to be presented here.   

\subsection{Many-Core Operating System Design}
\label{sec:many-core-os}

As the bottom layer of the software stack, operating systems have been
    re-examined recently in light of advances of hardware such as
    multicore CPUs and heterogeneous processors.
These are commonly called ``many-core'' platforms.
Much research in this area focuses on redesign from the ground-up,
    which means building a totally new operating system that is
    designed with many-core platforms in mind.
Examples of this kind of system include Corey~\cite{Corey},
    K42~\cite{K42}, Tornado~\cite{Tornado}, fos~\cite{fos},
    Barrelfish~\cite{Barrelfish}, and Helios~\cite{Helios}.
The main problem most of these systems focus on is optimizing resource
    sharing for performance, or more specifically, optimizing memory
    sharing.
Tornado and K42 partition OS data objects and cluster them into
    ``clustered objects'' to reduce sharing.
Corey reduces sharing through application-specified data sharing
    requirements. These three assume a base of shared memory.
The rest, including fos, Barrelfish, and Helios, use message-passing
    to offer ``shared-nothing'' mechanisms for minimal sharing and
    hence much higher parallelism.
In addition, there are projects that aim to make existing operating
    systems operate with more concurrency on homogeneous multi-core
    systems using techniques such as optimizing
    locks~\cite{Linux-scalability}.    
In contrast to these many-core OS designs, we aim to integrate
    parallel GPU computing devices into \emph{existing} operating
    systems or system level software frameworks.  
Rather than designing from scratch, we focus on conquering obstacles
    in existing system softwares that prevent the efficient
    integration of GPU computing for performance acceleration.
Different from CPUs, a GPU is not a fully functional processor that is
    able to run an OS kernel.
Functionalities required to run a traditional OS such as virtual
    memory, interrupts, preemption, controllable context switching,
    and the ability to interact directly with I/O devices are missing
    on GPUs.
GPUs are simply not suited to the above designs that treat them as
    peers to traditional CPUs.
Instead, they are better suited for use as co-processors, as our
    proposed frameworks do when integrating them into system level
    software.    

\subsection{General Purpose GPU Computing}
\label{sec:gpgpu}

Today's GPUs do much more than render graphics.
Through frameworks such as CUDA~\cite{CUDA-GUIDE} and
    OpenCL~\cite{OpenCL-GUIDE}, they can run general-purpose programs.
General purpose GPU computing (GPGPU) has been widely used in
    scientific computing, data visualization, database management
    systems, data mining, video processing, cryptography and many
    other fields~\cite{GPU-Survey, GPU-Crypto, GPU-Database}.
With their hundreds or thousands of SIMD cores, GPUs can excel at
    highly parallel data-intensive computation.
In addition to compute cores, GPUs also have on-board memory that is
    separate from the system's main memory.
Data must be copied back and forth between these two memories,
    resulting in overhead that must be carefully managed.
GPU memory provides software-managed local buffers near the cores,
    which offers spatial locality but also increases programming
    complexity.

\subsubsection{GPU Computing Frameworks}
Besides the two major fundamental frameworks, CUDA and OpenCL, there
    have been several research projects to ease the task of GPU
    programming.
hiCUDA~\cite{hiCUDA} provides high level abstractions to simplify
    the complex work of GPGPU programming, such as identifying and
    extracting GPU computation, managing GPU memory, and data copy.
The asymmetric distributed shared memory designed by Gelado
    et~al.~\cite{ASDM} eases memory management for GPU computing by
    eliminating redundant memory pointers on both the host and GPU.
Sponge~\cite{Sponge} provides a compilation framework that
    concentrates on optimizing data-flow management. 
Hydra~\cite{Hydra}, unifies heterogeneous computing devices, and
    could, in theory, be used to run GPU code from CPU-based systems;
    however, it depends on functionality that current GPUs do not
    support.
rCUDA~\cite{rCUDA} enables remote GPU operations with networked
    framework so that even hosts without GPU devices can run GPGPU
    programs.
GPUfs~\cite{GPUfs} provides file-system APIs for the code running on
    GPUs to ease the GPU programming with file I/O.
All of these frameworks or tools are aimed at simplifying GPGPU
    programming for traditional workflows.
While our research draws inspiration from these systems, it seeks
    instead to provide specialized support and resource management for
    a specific class of workflows, those found in system programming.

\subsubsection{Operating System Support of GPU Computing}    
Some efforts treat GPU execution as first-class elements of an OS.
TimeGraph~\cite{TimeGraph} is a GPU execution scheduler for real-time
    environments.
TimeGraph provides isolation and priority support for GPU operations,
    but is only for traditional graphics functions; general purpose
    GPU execution has more complex workflows.
PTask~\cite{PTask} presents a GPU task abstraction derived from UNIX
    pipes that are used to composite a dataflow graph.
Efficient memory management and concurrent execution are achieved by
    taking the dependencies in the graph into account.
Compared with our \gpustore{} framework, PTask requires
    modifying an existing storage system to use a pipe-like model,
    which would necessitate significant fundamental changes to its
    architecture.
Different from our \snap{} framework, PTask only deals with asyclic
    data flow among more coarse-grained process rather than much
    finer-grained Click elements~\cite{Click}.
A PTask processe consumes the input data and produces one or more
    \emph{new} streams of output data, but packet processing in
    \snap{}, in contrast, passes the \emph{same} data, packets,
    through a series of elements.
    
Gdev~\cite{Gdev} and Barracuda~\cite{brinkmann} bring GPU computing
into the OS kernel.  Barracuda uses a microdriver-based design and a
userspace helper, but provides only a bare minimum synchronous API
with no request scheduling, merging, splitting, or memory management.
Gdev provides a ``native'' in-kernel CUDA library as well as a
lower-level API. Gdev and our storage work on \gpustore{} are somewhat
complementary: Gdev concentrates on providing basic access to the GPU
from the kernel while \gpustore{} focuses on higher-level access
tailored specifically for storage systems.

\subsubsection{System Level GPU Computing}
\label{sec:sys-gpu-computing}

Here, we review recent applications of GPU to system level tasks.
Each of them uses its own ad-hoc mechanism to provide APIs to each
    particular system program and manage GPU resources and execution.
The goal of our proposed work is to provide general frameworks for
    the major categories of system level software rather than a
    particular application.

\begin{description}    
\item[Network Packet Processing:]
Recently, the GPU has been demonstrated to show impressive performance
enhancements for software routing and packet processing.
PacketShader~\cite{PacketShader} is capable of fast routing table 
lookups, achieving a rate of close to 40Gbps for both IPv4 and IPv6 
forwarding and up to 4x speedup over the CPU-only forwarding.
For IPSec, PacketShader gets a 3.5x speedup over the 
CPU. Additionally, a GPU-accelerated SSL implementation,
SSLShader~\cite{SSLShader} runs four times faster than an equivalent
CPU version. Compared with specialized network processors,
GPUs are lower cost and much more widely deployed.
Gnort~\cite{Gnort} and Kargus~\cite{kargus} utilize GPU-accelerated
    pattern matching algorithms in the Snort~\cite{snort} network
    intrusion detection system (NIDS).
There are also specially designed GPU-based IP lookup
    engine~\cite{ANCS13:GPU-IPLookup} and line rate name resolution
    engine~\cite{Wang+:NSDI13} that just focus on a very limited
    functionality.
Compared with our \snap{} work described in Section~\ref{sec:network},
    these all focus on offloading specific processing tasks to the
    GPU. They are not well suited to building more complex and generic
    network packet processing pipelines than a single processing function.   
\snap{} draws inspiration from this work, and seeks to simply and efficiently
    integrate GPU processing into complex, fully-functional routers and other
    packet processors.
\snap{} will be an enabler for future work in this area.

\item[GPUs in Storage Systems:]
Curry et~al. introduced Gibraltar, a software RAID infrastructure that
uses a GPU to perform Reed Solomon coding~\cite{GPU-RAID}.  It allows
for parity-based RAID levels that exceed the specifications of RAID~6,
increasing the resiliency of the array.  Gharaibeh et~al.\ implemented
GPU-accelerated hashing algorithms for content-addressed storage
systems~\cite{CrystalGPU}.  Bhatotia et~al.~\cite{GPU-IncStor}
presented Shredder, a framework specially designed for content-based
chunking to support incremental storage and computation.  These
represent specific instances of GPU computing applied to storage, and
are good examples of the benefits that can be gained from GPUs in
storage systems. Our storage framework \gpustore{} that is described
in Section~\ref{sec:storage} is a general framework, designed to aid
the implementation of applications like these and others.

\item[Other System Level Tasks:]
In addition to network and storage, the two main types of system level
tasks, there are some interesting potential uses of GPU computing in
computational expensive system level tasks.  These applications can
further strengthen the fundamental motivation of the proposed work by
showing the feasibility and effectiveness of the GPU-accelerated
functionality in various system level tasks.  Kaspersky Lab utilized
GPUs to enhance their anti-virus software~\cite{gpu-antivirus} with
two orders of magnitude speedup.  A patent application~\cite{GPU-GC}
shows it is possible to do the memory garbage collection on GPUs with
    better performance than 
on the CPU.  Recent work on EigenCFA has shown that the control flow
analysis (specifically, 0CFA) can be dramatically sped up using a
GPU~\cite{EigenCFA}.  With speedups like this, analysis that was
previously too expensive to do at load time or execution time becomes
more feasible; it is conceivable that some program analysis could be
done as code is loaded into the kernel or executed in some other
trusted context.

\end{description}
