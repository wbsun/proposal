\section{Harnessing GPUs in Storage Systems}
\label{sec:storage}

\subsection{Overview}
In this section, we describe our \gpustore{} work, a framework for
    integrating parallel GPU computing into storage systems.
More details are in the \gpustore{} conference paper~\cite{GPUstore}.
As one of the main functionality provided by system level softwares,
    storage can include many computationally expensive features.
Examples include encryption for confidentiality, checksums for
    integrity, and error correcting codes for reliability.
As storage systems become larger, faster, and serve more clients, the
    demands placed on their computational components increase and they
    can become performance bottlenecks.
Many of these computational tasks are inherently parallel: they can be
    run independently for different blocks, files, or I/O requests.
This makes them a good fit for GPUs.
However, because the software frameworks such as CUDA and OpenCL built
    for GPUs have been designed primarily for the long-running,
    data-intensive workloads seen in graphics or high-performance
    computing, they are not well-suited to the needs of storage
    systems.
\gpustore{} is designed to match the programming and computing models
    already used these systems.
We have prototyped \gpustore{} in the Linux kernel and demonstrate its use
    in three storage subsystems: file-level encryption, block-level
    encryption, and RAID~6 data recovery.
Comparing our GPU-accelerated drivers with the mature CPU-based
    implementations in the Linux kernel, we show performance
    improvements of up to an order of magnitude.
Different from the GPU computing work for storage described in
    Section~\ref{sec:sys-gpu-computing}, \gpustore{} is a general
    framework to aid the implementation of those applications and
    others rather than specially designed for a particular storage
    application. 
    
\subsection{Design Challenges}
\label{sec:storage-design-challenges}
The goal of the \gpustore{} framework is to allow the GPU to be used as a
    ``co-processor'' for storage subsystems, fitting naturally into
    the abstractions and patterns common to this domain.
Storage subsystems should be able to call functions that run on the
    GPU as easily as they call functions from any other subsystem in
    the kernel.
\gpustore{} was designed according to requirements and challenges from
    typical storage systems with the following computational tasks:
\begin{compactitem}
\item \textbf{Security and Confidentiality}: eCryptfs,
  Cryptfs~\cite{Cryptfs}, CFS~\cite{CFS}, \dmcrypt{},
  and more
\item \textbf{Reliability and Integrity}: ZFS~\cite{ZFS},
  software RAID (such as Linux's \md{}),
  $\textrm{I}^3$FS~\cite{I3FS}, etc.
\item \textbf{Performance}: Venti~\cite{Venti}, content-addressable storage
  such as HydraFS~\cite{HydraFS} and CAPFS~\cite{CAS-eg1}, etc.
\item \textbf{New Functionality}: in-kernel 
  databases~\cite{inkerneldb}, steganographic
    filesystems~\cite{StegFS},
  and more
\end{compactitem}
These storage subsystems fall into two main categories: filesystems,
such as eCryptfs and $\textrm{I}^3$FS, and virtual block device
drivers, such as \dmcrypt{} and \md{}.
After analyzing the properties of these two classes of components,
\gpustore{}'s design is guided by the identified five main factors.

\subsubsection{Asynchrony} 
\label{sec:asynchrony}
Filesystems often rely on the kernel page-cache for reading and
writing asynchronously unless an explicit \texttt{sync} operation or a
\emph{sync} flag.  Virtual block device drivers work with the kernel's
block-I/O layer, which is inherently an asynchronous request
processing system. Some network filesystems or block devices such as
NFS, CIFS, and iSCSI, depend on the inherently asynchronous network.
As a result, most filesystems and block devices work asynchronously to
avoid blocking and waiting.  In the meanwhile, the two main operations
in GPU computing: kernel launching and memory copy, both can be invoked
asynchronously (eg. through CUDA streams), but only synchronous
wait-for-completion APIs are provided by CUDA and OpenCL.

\subsubsection{Redundant Buffering} 
\label{sec:double-buffer}
GPU drivers allocate and maintain pages in main memory for the purpose
of copying data back and forth to the GPU.  At the same time, the
memory used for block devices and filesystems is managed (or sometimes
allocated) by the kernel block I/O layer or page cache.  This leads to
redundant buffers: in order to move data from a client subsystem to
the GPU, it must be copied from a source buffer to the GPU driver's
memory, and the results must similarly be copied back.  The CUDA
framework has a feature known as ``GPU-Direct
technology''~\cite{GPUDirect} to avoid redundant memory buffers
between the GPU driver and certain device drivers, but this is only
available to a select set of device drivers and not to general kernel
components.

\subsubsection{GPU Memory Copy and Access Overhead} 
\label{sec:need-large-request}
Good performance in a GPU program usually requires operating on a
large dataset~\cite{CUDA-GUIDE} to amortize the GPU kernel launching
overhead and the PCIe DMA overhead for memory copy, to fully utilize
thousands of GPU cores, and to hide the GPU's on-board memory access
latency by having enough threads for switching.
However, computing on large buffers is not the common case in the
Linux kernel's storage subsystems. Most of them work on single memory
page at a time or even smaller unit.
Modifying existing subsystems to enable large requests is possible but
may need fundamental and significant re-design, which is the proposed
research aims to avoid by bridging the gap between existing system
level software design and the needs of high performance GPU computing.

\subsubsection{Latency For Large Operations} 
\label{sec:small-request}
Fundamental GPU computing frameworks such as CUDA allows overlapped
    memory copy and kernel execution to enable a more efficient
    pipeline-like GPU computing model.
Operating on large data blocks reduces GPU overhead as described in
    Section~\ref{sec:need-large-request}, but it also prolong the
    pipeline stage, which hurts the processing latency and throughput:
    if executing a single GPU-kernel at a time, memory copies and
    computation are serialized rather than pipelined.
Splitting large computations into smaller pieces for better pipelining
    will help, but selecting an appropriate size for GPU tasks is a
    delicate balancing act that depends on the particulars of the GPU
    and GPU-kernel in use.
    
\subsubsection{GPU Resource Management} 
GPUs have more resources than just computing cores: they also have
    on-board memory and many have multiple DMA engines.
There may be many subsystems using a GPU at once, and a single
    computer may contain multiple GPUs.
Though current GPUs do not support software thread-level scheduling on
    GPU cores, the CUDA stream~\cite{CUDA-GUIDE} model includes
    higher-level abstraction for computing cores and DMA copy engines.
To provide a general framework for GPU computing inside the
    kernel, these resources must be shared by all client subsystems,
    and managed by \gpustore{}. 

\subsection{Design and Implementation}
\label{sec:storage-design-implementation}

\begin{figure}
  \centering
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=\textwidth]{figs/general_arch.pdf}
    \caption{The Architecture}
    \label{fig:gpustore_arch} 
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{figs/gpustore_flow.pdf}
    \caption{Workflow With Example Applications.}
    \label{fig:gpustore_flow} 
  \end{subfigure}
  \caption{Overview of \gpustore{}}
  \label{fig:gpustore:overview}
\end{figure}

In \gpustore{}, GPU computing tasks are abstracted into ``GPU
    services'' that provide functionalities requested by storage
    applications.
A callback-based asynchronous request processing mechanism was
    designed for \gpustore{} to work with asynchronous storage
    applications discussed in Section~\ref{sec:asynchrony}.
The processing of each request to a GPU service includes three major
    steps:
	(1) copy buffers from main memory to the GPU;
	(2) invoke the GPU kernel corresponding to the requested service;
	(3) copy results back after completion.
These steps are performed sequentially for each request, but steps
    from different requests (either for the same service or different
    services) can proceed in parallel, pipelining the process.
Figure~\ref{fig:gpustore_arch} shows the architecture of the
    \gpustore{} framework and the context it resides in.
Figure~\ref{fig:gpustore_flow} shows the workflow of applications that
    use GPU computing via \gpustore{}.
We will go through each major component of \gpustore{} to explain how
    the rest four challenges listed in
    Section~\ref{sec:storage-design-challenges} are solved.

\subsubsection{Request Management}
\label{sec:request-management}
To composite large requests to amortize the GPU overhead described in
    Section~\ref{sec:need-large-request}, and also to efficiently
    utilize the pipelined GPU computing model as described in
    Section~\ref{sec:small-request}, we borrowed ideas from the Linux
    I/O scheduler to schedule GPU services requests with \emph{merge}
    and \emph{split} operations.
As their names implied, \emph{merge} is used to composite large
    requests from a set of small ones for amortizing GPU overhead; and
    \emph{split} is used to partition large requests into small pieces
    for efficient pipeline utilization.
Detailed description of these two operations can be found in the
    \gpustore{} paper~\cite{GPUstore}.

Both \emph{merge} and \emph{split} need service-specific knowledge to
determine appropriate data sizes and number of requests: thresholds
are dependent on factors such as the relative costs of memory copies
and computation for a particular service.  \gpustore{} allows services to
implement their own request management and scheduling policies, such
as the number of requests to be batched for \emph{merge}s and the
maximum size for requests before they are \emph{split}.  These
policies can be specified statically in the service implementation or
defined dynamically; for example, by running small benchmarks at boot
time to customize them for particular GPUs.

This request scheduling allows existing storage application to use GPU
    computing without need of re-architecting to satisfy the data size
    requirements of efficient parallel GPU computing.
Besides \emph{merge} and \emph{split}, \gpustore{} provides even more
    flexible request scheduling policy to enable services to run in
    \emph{hybrid mode}: each service may set a size threshold for
    requests; below this threshold, the service is executed on the
    CPU.
This \emph{hybrid mode} is very useful when a CPU version of service
    outperforms its GPU counterpart for small data size.    
    
\subsubsection{Memory Management}
\label{sec:gpustore:mm}

\gpustore{} manages both the on-board GPU device memory and the main
memory allocated for DMA.  we use CUDA's mapped page-locked
memory~\cite{CUDA-GUIDE} to bind each buffer in main memory to a
corresponding buffer of the same size on the GPU device.  This
one-to-one mapping simplifies device memory management and gets the
highest performance for memory copies~\cite{CUDA-GUIDE}.

In order to remove the redundant buffer overhead described in
    Section~\ref{sec:double-buffer},
\gpustore{} supports two different approaches to client applications'
    memory allocation.
Each is useful in different types of code.
When the client subsystem manages its own buffers in memory,
    \emph{direct allocation} is appropriate.
When the client operates on buffers that are allocated and managed by
    another kernel subsystem (such as the page cache),
    \emph{remapping} eliminates redundant buffering for pre-existing
    allocations.
Both of them are realized through memory page remapping within the
    Linux kernel for the GPU drivers.    
Details of these two approaches are described in the \gpustore{} paper.
The main idea of these two approaches is to allow storage applications
    to read and write their common work data directly in memory pages
    that are capable of DMA to and from the GPU device memory, so as
    to avoid the redundant buffers.

\subsubsection{GPU Stream Management}
Though not very accurate, CUDA ``streams''~\cite{CUDA-GUIDE} can be
    treated as abstractions of the pipelined GPU computing.
\gpustore{} uses streams for asynchronous request process, high
    performance memory copy and scheduling the overlapped pipeline
    execution.
Although it is not currently possible to do software-controlled
    preemption on GPUs, a stream still represents a non-preemptible
    computing or copy resource.
\gpustore{} maintains GPU streams and assigns them to service requests
    for service execution and memory copies.
Our current \gpustore{} implementation uses a simple
    first-come-first-served policy to schedule streams.
However, it wold be straightforward to adopt other scheduling
    algorithms.

\subsubsection{Implementation}
\label{sec:gpustore:implementation}
We enhanced three existing Linux storage subsystems: encrypted storage
    with \dmcrypt{} and eCryptfs, and the software RAID driver \md{}.
We chose these three subsystems because they interact with the kernel in
    different ways: \md{} and \dmcrypt{} implement the block I/O interface, and
    eCryptfs works with the virtual filesystem (VFS) layer.
More implementation details are in the \gpustore{} paper.
Here we just list the lines of code changes required to use GPU
    computing with \gpustore{} to show the easy GPU computing
    integration enabled by \gpustore{} as a general framework.
\begin{table}
    \centering
    \begin{tabular}{|l|r|r|r|}
        \hline
        Subsystem & Total LOC & Modified LOC & Percent \\
        \hline \hline
        \dmcrypt{} &  1,800 &  50 & 3\% \\ \hline
        eCrytpfs   & 11,000 & 200 & 2\% \\ \hline
        \md{}      &  6,000 &  20 & 0.3\% \\ \hline
    \end{tabular}
    \caption{Approximate lines of code required to call GPU services
    using \gpustore{}.} 
    \label{table:gpustore:locs} 
\end{table}
The design of \gpustore{} ensures that client subsystems need only
    minor modifications to call GPU services.
Table~\ref{table:gpustore:locs} gives the approximate number of lines
    of code that we had to modify for our example subsystems.    

\subsection{Evaluation}
We did three categories of evaluation: the \gpustore{} framework
    performance, the GPU service performance evaluation, and the
    application evaluation.
More details of the evaluation and results are in the \gpustore{} paper.    

The framework was evaluated to reveal an upper bound for performance
    or GPU services, since it was evaluated with no computation.
It also shows a 30\% performance boost with our asynchronous request
    processing design, and the effectiveness of our \emph{merge}
    request scheduling operation.
    
In the GPU service evaluation, the AES cipher GPU service and the RAID
    6 recovery GPU service were evaluated, each show a maximum 36x
    speedup and a maximum 6x speedup over their CPU-based counterparts
    respectively.
The GPU service performance evaluations show the effects of our
    optimization to remove redundant buffering and the \emph{split}
    operation.
    
We evaluated the three modified applications on both a fast SSD and
    the RAM disks.    
Our results show that GPU can saturate our SSD's maximum throughput
    while the CPU is far away from the SSD's throughput limit.
When on RAM disks, the GPU-based storage applications can show
    tremendous speedup over the CPU-based ones, which is able to
    achieve the read/write rate of Fusion I/O's ioDrive Duo, the third
    fastest SSD in production~\cite{ssd-ranking} at the time of the
    \gpustore{} paper writing.
To help readers get the idea, we just put the \dmcrypt{} results in
    Figure~\ref{fig:dm:result}.     
We also evaluated eCryptfs under concurrent workload to see the
    scalability a GPU-accelerated storage system can achieve.
Our result, as shown in Figure~\ref{fig:gpustore:ecryptfs:ram:cw},
demonstrates that \gpustore{} is also useful for workloads with many
    simultaneous clients.
    
All these three evaluations demonstrate the effectiveness of
    the designs described in
    Section~\ref{sec:storage-design-implementation}.

\begin{figure}
  \centering
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=\textwidth]{figs/dm_ssd.pdf}
    \caption{Result on SSD}
    \label{fig:dm:ssd} 
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\textwidth]{figs/dm_ram.pdf}
    \caption{Result on RAM Disks}
    \label{fig:dm:ram} 
  \end{subfigure}
  \caption{Throughputs of \dmcrypt{} on Different Devices}
  \label{fig:dm:result}
\end{figure}    

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{figs/ecryptfs_ram_cw}
\caption{eCryptfs concurrent write throughput on RAM disk.}
\label{fig:gpustore:ecryptfs:ram:cw}
\end{figure}
  

\subsection{Summary}
\gpustore{} represents the effort we've made towards a general GPU
    computing framework for storage applications.
We designed \gpustore{} around common storage paradigms so that it is
    simple to use from existing code, and have enabled a number of
    optimizations that are transparent to the calling system.    
The implementation and evaluation show that we are able to achieve
    substantial improvement in performance by introducing very small
    amount of modifications in existing storage systems with
    \gpustore{}.

    
