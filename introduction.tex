\section{Introduction}
\label{sec:introduction}

System level software, such as file-systems, device drivers, network
    stacks, and other operating system components are the base of the
    application software stack.
They provide basic storage, communication, security and other
    essential functionalities for the upper level applications. 
These functionalities are expected to be efficient and scalable.
Yet, many features provided by the system level software can require
    substantial computing power.
Examples include: encryption/decryption for privacy; deep inspection
    of packets for network protection; error correction code or
    erasure code for fault tolerance; and lookups in large data
    structures (such as file-systems, routing tables, or memory
    maps).
All of these can consume excessive processing power, slowing down the
    rest of the software stack.
What is more, today's application workloads are dramatically
    increasing in size: gigabytes or terabytes of
    multimedia content, high definition photos and videos, tens or
    even hundreds giga-bits per second network traffic, and so on.
Thus more and more computing power is demanded to execute those
    bulk-data workloads withe modern rich functional software stacks.

Many of these computational tasks in system level software are
    inherently parallelizable.
Processing can be done in parallel on the granularity of memory pages,
    network packets, blocks in I/O devices, etc. 
Exploiting these kinds of parallelism to take advantage of today's
    parallel processor advancement is valuable to satisfy the
    excessive demand of computing power by modern bulk-data workloads.
Highly parallel processors, in the form of GPUs, are now common in a
    wide range of systems, from mobile devices up to cloud
    servers~\cite{AWS-GPU, GTC13:GRID} and
    super computers~\cite{SC:Titan, GPU-HPC}.
Modern GPUs provide far more parallel computing power than multi-core
    or many-core CPUs: while a CPU may have two to eight cores, a
    number that is creeping upwards, a modern GPU may have over two
    thousand~\cite{Titan}, and the number of cores is roughly
    doubling each year \cite{CUDA-GUIDE}.
GPUs are designed as the \emph{throughput-oriented
    architecture}~\cite{TOA}: thousands of cores work together to
    execute large amount of parallel workloads, attempting to maximize
    the total throughput, by sacrificing the serial performance.
Though each single GPU core is slower than a CPU core, when the
    computation task at hand is highly parallel, GPUs can provide
    dramatic improvements in throughput.
This is especially efficient to process the bulk-data workloads, which
    often bring more parallelism to fully utilize the thousands of GPU
    cores.   

GPUs are leading the way in parallelism: compilers~\cite{gpu-comp1,
    gpu-comp2, gpu-comp3}, algorithms~\cite{gpu-algo1, gpu-ds1,
    gpu-algo2}, and computational models~\cite{kmodel, model2,
    a-model} for parallel code have made significant advances in
    recent years.
High level applications such as video processing, computer graphics,
    artificial intelligence, scientific computing and many other
    computational expensive and large scale data processing tasks have
    benefited with significant performance
    speedups~\cite{GPU-Survey, GPU-SurveyNew} from the advancement
    of parallel GPUs.
System level software, however, have been largely left out of this
    revolution in parallel computing.    
The major factor in this absence is the lack of technologies for how
    system level software should be mapped to and executed on GPUs.
    
In high level software, GPU computing is fundamentally based on a
    computing model derived from graphics processing and high
    performance computing (HPC).
They are designed for long-running computation on large datasets, as
    seen in graphics and HPC.
In contrast, system level software is built, at the lowest levels of
    the software stack, on sectors, pages, packets, blocks, and other
    relatively small structures despite of modern bulk-data
    workloads.
The computation request required on each of such small structures is
    relatively modest.
Apart from that, system level software also has to deal with very low
    level computation elements, such as memory management involving
    pages, caches, and page mappings, block I/O scheduling, complex
    hardwares, device drivers, fine granularity performance or memory
    optimizations and so on, which are often hidden from the high
    level software.
As a result, system level software requires technologies: to bridge the
    gap between the small building structures and the
    large datasets oriented GPU computing model; also to properly
    handle those low level computation elements with careful design,
    trade-offs, and optimizations, to take advantage of the 
    parallel throughput-oriented GPU architecture.
    
%% As we will discuss in Section~\ref{sec:background}, there is now
    %% significant evidence in the literature that the combination of
    %% system-level computing and GPUs can have substantial benefits: a
    %% number of separate efforts, each using its own very specialized
    %% ah-hoc approach for GPU execution, have been published.
%% The open challenge, then, is to create a general model of system level
    %% GPU computing.
%% Such a model has the potential to open the door to a new field of
    %% research through better integration of parallel computing
    %% throughout the software stack, enabling addition of new features
    %% and acceleration of existing ones.

We propose to investigate the design, implementation and evaluation of
    the general throughput-oriented GPU computing models for two
    representative categories of system level software:
    \emph{\textbf{storage}} applications, and \emph{\textbf{network}}
    packet processing.
Both models are in the form of general frameworks that are designed
    for seamlessly and efficiently integrating parallel GPU computing
    into a large category of system level software.
Our contributions will be two-fold:
\begin{itemize}
  \item A survey of existing and potential system level GPU computing
    applications. Though there have been efforts of combining
    system level computing and GPUs for performance improvement in the
    literature, system level GPU computing still has not gained enough
    attention yet. We will try to survey the currently existing
    system level GPU computing applications, and also identify more
    potential system level applications that can possibly be
    accelerated with GPUs. Such a survey can open new opportunities
    for system software research, providing a reference answer of ``what'':
    \emph{What kinds of system level software can be accelerated by
    GPUs?}.
  \item Frameworks for storage and network packet processing
    applications to integrate parallel GPU computing. The contribution
    is not only the general frameworks for these two representative
    kinds of system level software, but also the challenges we
    identified and solved, the shared architectural and optimization
    designs and trade-offs by these two frameworks, and the lessons we
    learned from the process of designing, implementing and evaluating
    the frameworks.  We believe they are also the common design
    choices of and valuable to GPU computing in other kinds of
    system level software. This answers the ``how'': \emph{How can the
    system level software be accelerated by GPUs}.
\end{itemize}
Hence the thesis statement is:
\begin{quote}
  \emph{The throughput of the system level software can be improved by
    harnessing parallel GPUs with the efficient and carefully designed
    GPU computing frameworks.}
\end{quote}

The rest of the proposal is organized into four parts.
First, we review the related work
    in Section~\ref{sec:background}.
Second, we describe \gpustore{}, the GPU computing framework we
    designed for storage components in Linux kernel in
    Section~\ref{sec:storage}.
Third, we describe \snap{}, our flexible and modular GPU computing
    framework for network packet processing in
    Section~\ref{sec:network}.
And last, we propose the dissertation work plan in
    Section~\ref{sec:plan}.
