## -*- mode: Makefile -*-

all: doc

doc:: *.tex *.sty
	pdflatex proposal
	bibtex proposal
	pdflatex proposal
	pdflatex proposal

clean::
	$(RM) *.aux *.bbl *.blg *.dvi *.log *.out *.synctex.gz

realclean:: clean
	$(RM) *.eps *.pdf *.ps

############################################################################

## End of file.
