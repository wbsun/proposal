\section{Fast and Flexible Packet Processing With GPUs}
\label{sec:network}

\subsection{Overview}
As networks advance, the need for high-performance packet processing
    in the network increases for two reasons: first, networks get
    faster, and second, we expect more functionality from
    them~\cite{wan-optimizer,openflow,IDS,
    Anderson+:HotNets03,XIA,packet-cache}.
The nature of packet data naturally lends itself to parallel
    processing~\cite{routebricks}, and recent work has demonstrated
    that GPUs, which have thousands of cores, are well-suited to a
    number of packet-processing tasks.
These include, but are not limited to, route
    lookup~\cite{PacketShader}, encryption~\cite{SSLShader}, and deep
    packet inspection~\cite{Gnort, kargus}.
However, a software router is made up of more than just these
    heavyweight processing and lookup elements.
A range of other elements are needed to build a fully functional
    router, including ``fast path'' elements such as TTL decrement,
    checksum recalculation, and broadcast management, and ``slow
    path'' elements such as handling of IP options, ICMP, and ARP.
Building new features not present in today's routers adds even more
    complexity.
To take full advantage of the GPU in a packet processor, what is
    needed is a flexible, modular framework for building complete
    processing pipelines by composing GPU programs with each other and
    with CPU code.

In this section, we describe our \snap{} work, a general purpose
    network packet processing framework with parallel GPU computing
    integration to address this need.
Detailed \snap{} description can be found in the \snap{} conference
    paper~\cite{Snap}.
\snap{} extends the architecture of the Click modular
    router~\cite{Click} to support offloading parts of a packet
    processor onto the GPU.
\snap{} enables individual elements, the building blocks of a Click
    processing pipeline, to be implemented as GPU code.
It extends Click with ``wide'' ports that pass batches of packets,
    suitable for processing in parallel, between elements.
\snap{} also provides elements that act as adapters between serial
    portions of the processing pipeline and parallel ones, handling
    the details of batching up packets, efficiently copying between
    main memory and the GPU, scheduling GPU execution, and directing
    the outputs from elements into different paths on the processing
    pipeline.
In addition to these user-visible changes to Click, \snap{} also makes
    a number of ``under the hood'' changes to the way that Click
    manages memory and optimizes its packet I/O mechanisms to support
    multi-10\,Gbit rates.
We show that \snap{} can run complex pipelines at high speeds on
    commodity PC hardware by building an IP router incorporating both
    an IDS-like full-packet string matcher and an SDN-like packet
    classifier.
In this configuration, \snap{} is able to forward 40~million packets
    per second, saturating four 10~Gbps NICs at packet sizes as small
    as 128 byes.
This represents an increase in throughput of nearly 4x over the
    baseline Click running comparable elements on the CPU.    

\subsection{Design and Implementation}
\label{sec:snap:design-implement}

We designed \snap{} with two goals in mind: enabling \emph{fast}
    packet processors through GPU offloading while preserving the
    \emph{flexibility} in Click that allows users to construct complex
    pipelines from simple elements.
\snap{} is designed to offload specific elements to the GPU: parts of
    the pipeline can continue to be handled by existing elements that
    run on the CPU, with only those elements that present
    computational bottlenecks re-implemented on the GPU.
From the perspective of a developer or a user, \snap{} appears very
    similar to regular Click, with the addition of a new type of
    ``batch'' element that can be implemented on the GPU and a set of
    adapter elements that move packets to and from batch elements.
Internally, \snap{} makes several changes to Click in order to make
    this pipeline work well at high speeds.
We will just walk through a few interesting design and implementation problems
    and solutions of \snap{} in the following text, leave more
    essential and important \snap{} design choices and considerations
    in the paper.

\subsubsection{Batched Processing}
\label{sec:snap:batch}
As we described in Section~\ref{sec:need-large-request}, a common
    performance requirement of GPU computing is the need of large data
    set to amortize the overhead, and to provide enough parallelism
    to fully utilize the thousands of GPU cores.
So an essential design of \snap{} is to enable batched processing in
    Click by widening its pipeline, which is composited by elements
    that process one packet each time.
We added wider versions of Click elements' \push{} and \pull{}
    interfaces, \bpush{} and \bpull{} to exchange the batched packets,
    which are represented by a \packetbatch{} structure instead of a
    single packet.
A new element class, \belement{} is also derived from Click's standard
    \element{} class to represent elements with such wider interfaces.
The change exposed to GPU packet processing developers is minor:
    simply deriving from \belement{} instead of \element{}, and
    overriding \bpush{} or \bpull{} rather than \push{} or \pull{}.

To accumulate packets into a \packetbatch{}, we take a cue from
    Click's \queue{} elements to design a new element, \batcher{},
    which collects packets one at a time from a sequential \element{}
    on one side and pushes them in batches to a \belement{} on the
    other side.
A \debatcher{} element performs the inverse function. 
Implementing this functionality as a new element, rather than changing
    Click's infrastructure code, can minimize the changes within
    Click, make batching and offloading fully under the control of the
    creator of the \snap{} configuration---while we provide
    carefully-tuned implementations of \batcher{} and \debatcher{}
    elements, it is possible to provide alternate implementations
    designed for specific uses (such as non-GPU \belement{}s) without
    modifying \snap{}.
Many details of data structure design, performance optimization and
    design considerations are ignored here, readers should refer to
    the \snap{} paper~\cite{Snap} for more information.

\subsubsection{Packet Slicing}
\label{sec:snap:slicing}

Many packet processing algorithms, such as Ethernet switching, IP
    route lookup, packet classification, and IP header checksum
    computation, need only a portion of the entire packet.
As a common case, many need only protocol headers.
This means that when offloading, it is not always necessary to copy
    the entire packet to device memory.
To take advantage of this situation, we designed a slicing mechanism
    in \packetbatch{} to optimize the host-to-device packet copy
    performance.
\begin{figure}
  \centering
  \includegraphics[width=0.6\textwidth]{figs/slicing.pdf}
  \caption{A Slicing Example}
  \label{fig:slicing}
\end{figure}    
Slicing (illustrated in Figure~\ref{fig:slicing}), allows GPU
    processing elements to specify the regions of packet data that
    they operate on, called Regions of Interest (ROIs).
\batcher{} accepts ROI requests from its downstream \belement{}s and
    takes their union to determine which parts of the packet must be
    copied to the GPU.
It allocates only enough host and device memory to hold these ROIs,
    and during batching, \batcher{} only copies data in these regions
    into a packet's host memory buffer.
This reduces both the memory requirements for \packetbatch{}es and the
    overhead associated with copying them to and from the GPU.
For element developers' convenience, we have provided helper APIs for
    \belement{}s that allow the element to address packet data
    relative to its ROIs---the true offsets within the \packetbatch{}
    are computed transparently.

\subsubsection{Packet Divergence}
\label{sec:snap:divergence}

\snap{} faces a problem not encountered by other GPU processing
    frameworks~\cite{PacketShader,kargus,Gnort}, namely the fact that
    packets in Click do not all follow the same path through the
    \element{} graph.
\element{}s may have multiple output \port{}s, reflecting the fact
    that different packets get routed to different destination NICs,
    or that different sets of processing elements may be applied
    depending on decisions made by earlier elements.
This means that packets that have been grouped together into a
    \packetbatch{} may be split up (diverged) while on the GPU or
    after being returned to host memory.
For divergence that occurs on the GPU, our experiments show that the
    overheads associated with splitting up batches and copying them
    into separate, smaller \packetbatch{}es are prohibitive,
    especially in the case of exception-path divergence.
Assembling output \packetbatch{}es from selected input packets is also
    not concurrency-friendly: determining each packet's place in the
    output buffer requires knowledge of how many packets precede it,
    which in turn requires global serialization or synchronization.
Therefore, \snap{} attaches a set of \textit{predicate bits} to each
    packet in a \packetbatch{}---these bits are used to indicate which
    downstream \belement{}(s) should process the packet.
Predicates are stored as annotations in Click's \packet{} structure.
The thread processing each packet is responsible for checking and
    setting its own packet's predicate; this removes the need for
    coordination between threads in preparing the output.


\subsubsection{Speeding Up Packet I/O}
\label{sec:snap:pio}

Click includes existing support for integration with \netmap{}
    \cite{Netmap} for fast, zero-copy packet I/O from userspace.
We found, however, that Click's design for this integration did not
    perform well enough to handle the packet rates enabled by
    \snap{}.
Specifically: \snap{} uses a thread-safe packet memory pool for
    \netmap{} instead of previous single-threaded one; we also
    extended \netmap{} to provide userspace code a larger memory pool
    for zero-copied packets, to avoid starvation under high packet
    rate traffic; the well-known NIC's multiqueue (RSS/MQ) on
    multicore CPU technique that pins each NIC queue's thread to a
    specific core is also adopted into \snap{} for data locality.
Combined, these optimizations give \snap{} the ability to handle up
    to 2.4 times as many packets per second as Click's I/O code---this
    improvement was critical for small packet sizes, where the
    unmodified packet I/O path was unable to pull enough packets from
    the NIC to keep the processing elements busy.
    
\subsection{Evaluation}

We've evaluated \snap{} in many aspects:
\begin{compactitem}
  \item Basic packet I/O improvement evaluated with simple forwarding
    experiments, which shows $2.38x$ speedup with \snap{}.
  \item Application performance. We implemented three kinds of packet
    processing tasks: IP lookup, packet classifier for SDN, and
    pattern matching for intrusion detection. By combining these
    elements, we built three kinds of \snap{} configurations as
    applications for evaluation: SDN forwarder, DPI router, and IDS
    router.
  \item Packet latency and reordering.
  \item Packet divergence handling.
  \item Flexibility and modularity to build a fully functional IDS
    router.
\end{compactitem}
\begin{figure}
  \centering
  \includegraphics[width=0.4\textwidth]{figs/sdn-res.pdf}
  \caption{\snap{} SDN Forwarder Performance}
  \label{fig:snap:sdn}
\end{figure}
We just put the SDN forwarder result in Figure~\ref{fig:snap:sdn} as
    example to demonstrate the fast packet processing \snap{} has
    enabled with its various design features and GPU computing.
In the SDN forwarder configuration, \snap{} is able to achieve full
    line rate with four 10Gb NIC ports at small packet size (128
    bytes), which is about 4x speedup compared with the CPU-based
    packet processing configuration.

One important evaluation is to build a fully functional IDS router.
We demonstrate that \snap{} can be used to build not only highly
    specialized forwarders, but also a complete standards compliant IP
    router.
Specifically, we base our IP router off of the configuration shown in
    Figure~8 of the Click paper~\cite{Click}, which includes support
    for error checking of headers, fragmentation, ICMP redirects, TTL
    expiration, and ARP.
We replace the \clickel{LookupIPRoute} element with our
    \clickel{GPUIPLookup} element (and the accompanying \batcher{},
    etc.), and add an IDS element to both the CPU and GPU
    configurations.
\begin{figure}
  \centering
  \includegraphics[width=0.4\textwidth]{figs/full-router-perf.pdf}
  \caption{Fully functional IP router + IDS performance}
  \label{fig:snap:flex}
\end{figure}
The performance of the CPU-based and GPU-based full router
    configurations are shown in Figure~\ref{fig:snap:flex}.
This fully-functional router with built-in IDS is able to achieve 2/3
    of the performance of a trivial forwarder for minimum-sized
    packets, and almost full line rate (38.8\,Gbps) for 512-byte and
    larger packets.
This demonstrates the feasibility of composing complex graphs of CPU
    and GPU code, and shows that existing CPU Click elements can be
    easily used in \snap{} configurations without modification.

\subsection{Summary}
With carefully designed and implemented \snap{}, we are able to keep
    the flexibility and modularity of the original Click software
    router, and in the meanwhile \snap{} enables parallel GPU-based
    processing elements to build general high performance packet
    processing applications.
As a general GPU computing framework for packet processing, \snap{}
    has been evaluated to demonstrate its effectiveness and
    feasibility to easily develop and composite high performance
    packet processing software routers and switches.    
